import os

import stripe
from werkzeug.exceptions import InternalServerError

STRIPE_SECRET = os.environ.get("STRIPE_SECRET")

stripe.api_key = STRIPE_SECRET


def pay(amount, course_title, card_number, exp_month, exp_year, cvc):
    token = generate_token(card_number, exp_month, exp_year, cvc)
    return charge(course_title=course_title, amount=int(amount), token_id=token.id)


def generate_token(card_number, exp_month, exp_year, cvc):
    try:
        return stripe.Token.create(
            card={
                "number": card_number,
                "exp_month": exp_month,
                "exp_year": exp_year,
                "cvc": cvc,
            },
        )
    except Exception as e:
        raise InternalServerError("Internal Server")


def charge(course_title, amount, token_id):
    try:
        return stripe.Charge.create(
            amount=amount * 100,
            currency="eur",
            source=token_id,
            description=course_title,
        )
    except Exception as e:
        raise InternalServerError("Internal Server")
