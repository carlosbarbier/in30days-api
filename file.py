import os

import boto3

S3_BUCKET = os.environ.get("S3_BUCKET")
S3_KEY = os.environ.get("S3_KEY")
S3_SECRET = os.environ.get("S3_SECRET")
S3_ENDPOINT = os.environ.get("S3_ENDPOINT")
S3_REGION = os.environ.get("S3_REGION")

s3 = boto3.client('s3',
                  region_name=S3_REGION,
                  aws_access_key_id=S3_KEY,
                  aws_secret_access_key=S3_SECRET,
                  )


def upload_file(file_name):
    try:
        object_key = file_name
        response = s3.upload_file(file_name, S3_BUCKET, object_key)
        return response
    except Exception as e:
        print("Something Happened: ", e)
        return e


def delete_file(object_key):
    try:
        response = s3.delete_object(Bucket=S3_BUCKET, Key=object_key)
        return response
    except Exception as e:
        print("Something Happened: ", e)
        return e
