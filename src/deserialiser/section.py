from marshmallow_sqlalchemy import SQLAlchemyAutoSchema, fields

from src.model import Section


class SectionSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Section
        include_relationships = True
        load_instance = True
        exclude = ("created_at", "updated_at", "courses")

    videos = fields.Nested('VideoSchema', many=True, exclude=('videos',))
