from marshmallow_sqlalchemy import SQLAlchemyAutoSchema, fields, auto_field

from src.model import Course


class CourseSchemaWithSection(SQLAlchemyAutoSchema):
    class Meta:
        model = Course
        include_relationships = True
        load_instance = True
        exclude = ("created_at", "is_ready", "image_key", "course","course_id")

    sections = fields.Nested('SectionSchema', many=True, exclude=('courses',))


class CourseSchemaWithoutQas(SQLAlchemyAutoSchema):
    class Meta:
        model = Course
        load_instance = True
        exclude = ("created_at", "is_ready", "image_key")

    sections = fields.Nested('SectionSchema', many=True, exclude=("courses",))


class CoursesSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Course
        load_instance = True
        exclude = ("is_ready", "image_key")
