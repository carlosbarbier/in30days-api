from marshmallow_sqlalchemy import SQLAlchemyAutoSchema, auto_field

from src.model import StudentVideo


class StudentVideoSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = StudentVideo
        include_fk = True
        exclude = ("created_at", "updated_at", "id")
