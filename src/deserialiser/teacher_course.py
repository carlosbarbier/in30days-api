from marshmallow_sqlalchemy import SQLAlchemyAutoSchema, fields

from src.model import TeacherCourse


class TeacherCoursesSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = TeacherCourse
        include_relationships = True
        load_instance = True

    course = fields.Nested('CoursesSchema', )