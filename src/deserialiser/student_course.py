from marshmallow_sqlalchemy import SQLAlchemyAutoSchema, fields

from src.model import StudentCourse


class StudentCourseSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = StudentCourse
        include_relationships = True
        load_instance = True
        exclude = ("created_at", "updated_at", "is_paid", "is_active")
    course = fields.Nested('CoursesSchema', )
