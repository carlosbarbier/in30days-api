from marshmallow_sqlalchemy import SQLAlchemyAutoSchema, fields

from src.model import StudentQa


class StudentQaSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = StudentQa
        include_relationships = True
        load_instance = True
        exclude = ("created_at", "updated_at", "parent")

    replies = fields.Nested('StudentQaReplySchema', many=True)


class StudentQaReplySchema(SQLAlchemyAutoSchema):
    class Meta:
        model = StudentQa
        load_instance = True
        exclude = ("created_at", "updated_at")
