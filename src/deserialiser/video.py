from marshmallow_sqlalchemy import SQLAlchemyAutoSchema

from src.model import Video


class VideoSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Video
        include_relationships = True
        load_instance = True
        exclude = ("created_at", "updated_at","video_key")
