from flask import jsonify


class GlobalException:

    @staticmethod
    def handle(app):
        # 400 - Bad Request
        @app.errorhandler(400)
        def bad_request(e):
            return jsonify({'message': 'data not found'}), 400

        @app.errorhandler(403)
        def forbidden(e):
            return "403"

        @app.errorhandler(404)
        def page_not_found(e):
            return jsonify({'message': 'route not found'}), 404

        @app.errorhandler(405)
        def method_not_allowed(e):
            return jsonify({'message': 'http method not allow'}), 405

        @app.errorhandler(500)
        def server_error(e):
            return jsonify({'message': 'Internal server error'}), 500

        @app.errorhandler(503)
        def server_error(e):
            return jsonify({'message': 'Server unavailable'}), 503

        @app.errorhandler(422)
        def server_error(e):
            return jsonify({'message': 'Invalid data provided'}), 422

        @app.errorhandler(401)
        def server_error(e):
            return jsonify({'message': 'Invalid or expired token'}), 401
