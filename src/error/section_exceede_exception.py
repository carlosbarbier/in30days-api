from werkzeug.exceptions import HTTPException


class SectionExceedException(HTTPException):
    code = 400
    description = "you have already 30 sections  for this course"
