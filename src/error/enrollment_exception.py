from werkzeug.exceptions import HTTPException


class EnrollmentException(HTTPException):
    code = 401
    description = "you are not enrolled in this course"
