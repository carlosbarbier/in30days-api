from werkzeug.exceptions import HTTPException


class CourseExistException(HTTPException):
    code = 400
    description = "course title already exist"
