import http

from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource, reqparse

from src.decorator.auth import student
from src.service.student.rating_service import RatingService
from src.validation import rating_validator


class StudentRatingsResource(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser(bundle_errors=True)

    @jwt_required()
    @student()
    def post(self, slug):
        rating_validator(self.parser)
        args = self.parser.parse_args()
        current_student = get_jwt_identity()
        RatingService.save_rating(args=args, slug=slug, student_id=current_student['id'])
        return {}, http.HTTPStatus.OK
