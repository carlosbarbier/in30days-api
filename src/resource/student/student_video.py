import http

from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource

from src.decorator.auth import student
from src.service.student.video_service import VideoService


class StudentVideoResource(Resource):
    @jwt_required()
    @student()
    def get(self, course_id):
        current_student = get_jwt_identity()
        videos = VideoService.get_watched_videos(course_id, student_id=current_student['id'])
        return videos, http.HTTPStatus.OK


class StudentVideosResource(Resource):
    @jwt_required()
    @student()
    def post(self, slug, video_id):
        current_student = get_jwt_identity()
        VideoService.finish_to_watch_video(slug, video_id, student_id=current_student['id'])
        return {}, http.HTTPStatus.OK
