from flask_jwt_extended import jwt_required
from flask_restful import Resource

from src.decorator.auth import student


class DashboardResource(Resource):
    @jwt_required()
    @student()
    def get(self):
        try:
            return {}, 200
        except Exception as e:
            raise e
