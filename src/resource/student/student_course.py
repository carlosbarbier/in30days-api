import http

from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource, reqparse

from src.decorator.auth import student
from src.service.student.course_service import CourseService
from src.validation import card_validator


class StudentCourseResource(Resource):
    @jwt_required()
    @student()
    def get(self, slug):
        current_student = get_jwt_identity()
        return CourseService.get_course(current_student['id'], slug=slug)


class StudentCoursesResource(Resource):
    @jwt_required()
    @student()
    def get(self):
        current_student = get_jwt_identity()
        return CourseService.get_courses(current_student['id'])


class StudentFreeEnrollmentResource(Resource):
    @jwt_required()
    @student()
    def post(self, slug: str):
        current_student = get_jwt_identity()
        CourseService.enroll_for_free_course(slug, student_id=current_student['id'])
        return {}, http.HTTPStatus.OK


class StudentFinishCourseResource(Resource):
    @jwt_required()
    @student()
    def post(self, slug: str):
        current_student = get_jwt_identity()
        CourseService.finish_course(student_id=current_student['id'], slug=slug)
        return {}, http.HTTPStatus.OK


class StudentPaidEnrollmentResource(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser(bundle_errors=True)

    @jwt_required()
    @student()
    def post(self, slug: str):
        card_validator(self.parser)
        args = self.parser.parse_args()
        current_student = get_jwt_identity()
        CourseService.enroll_for_paid_course(args, slug, student_id=current_student['id'])
        return {}, http.HTTPStatus.OK
