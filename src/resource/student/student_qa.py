import http

from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource, reqparse

from src.decorator.auth import student
from src.service.student.qa_service import QaService
from src.validation import qa_validator


class StudentQasResource(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser(bundle_errors=True)

    @jwt_required()
    @student()
    def post(self, slug):
        qa_validator(self.parser)
        args = self.parser.parse_args()
        current_student = get_jwt_identity()
        QaService.save_qa(args=args, slug=slug, student_id=current_student['id'])
        return {}, http.HTTPStatus.OK

