from flask import jsonify
from flask_jwt_extended import create_access_token, set_access_cookies
from flask_restful import Resource, reqparse

from src.service import UserService
from src.validation import user_login_validator

parser = reqparse.RequestParser(bundle_errors=True)


class UserLoginResource(Resource):

    def post(self):
        user_login_validator(parser)
        args = parser.parse_args(http_error_code=422)
        user = UserService.find_user_by_email(args['email'], args['password'])
        response = jsonify()
        access_token = create_access_token(identity=user)
        set_access_cookies(response, access_token)
        payload = {"token": access_token, "user": {"id": user['id'], "role": user['role']}}
        response = jsonify(payload)
        return response
