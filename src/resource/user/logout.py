from flask import jsonify
from flask_jwt_extended import get_jwt, jwt_required
from flask_restful import Resource, reqparse

from cache import redis_con
from src.config.config import ACCESS_EXPIRES

parser = reqparse.RequestParser(bundle_errors=True)


class UserLogoutResource(Resource):
    @jwt_required()
    def post(self):
        jti = get_jwt()["jti"]
        redis_con.set(jti, "", ex=ACCESS_EXPIRES)
        return jsonify(msg="Access token revoked")
