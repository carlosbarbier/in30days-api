from flask_restful import Resource

from src.service.course_service import CourseService


class CoursesResource(Resource):

    def get(self):
        return CourseService.get_courses()


class CourseResource(Resource):

    def get(self, slug):
        return CourseService.get_course(slug)
