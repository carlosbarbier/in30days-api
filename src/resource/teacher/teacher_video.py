import http

from flask_jwt_extended import jwt_required
from flask_restful import Resource, reqparse

from src.decorator.auth import student, teacher
from src.service.teacher.video_service import VideoService
from src.validation import video_creation_validator, video_update_validator, videos_arrangement_validator


class TeacherVideosResource(Resource):

    def __init__(self):
        self.parser = reqparse.RequestParser(bundle_errors=True)

    @jwt_required()
    @student()
    def get(self):
        pass

    @jwt_required()
    @teacher()
    def post(self):
        video_creation_validator(self.parser)
        args = self.parser.parse_args()
        VideoService.save_video(args)
        return {}, http.HTTPStatus.CREATED


class TeacherVideoResource(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser(bundle_errors=True)

    @jwt_required()
    @teacher()
    def get(self, video_id):
        return VideoService.get_video(video_id)

    @jwt_required()
    @teacher()
    def put(self, video_id):
        video_update_validator(self.parser)
        args = self.parser.parse_args()
        VideoService.update_video(args, video_id)
        return {}, http.HTTPStatus.OK

    @jwt_required()
    @teacher()
    def delete(self, video_id):
        VideoService.delete_video(video_id)
        return {}, http.HTTPStatus.OK


class TeacherVideosPositionsResource(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser(bundle_errors=True)

    @jwt_required()
    @teacher()
    def put(self, slug):
        parser = reqparse.RequestParser()
        videos_arrangement_validator(parser)
        args = parser.parse_args(http_error_code=422)
        VideoService.arrange_videos_positions(slug, args)
        return {}, http.HTTPStatus.OK
