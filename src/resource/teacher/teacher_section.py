import http

from flask_jwt_extended import jwt_required
from flask_restful import Resource, reqparse

from src.decorator.auth import teacher
from src.service.teacher.section_service import SectionService
from src.validation import section_validator


class TeacherSectionsResource(Resource):

    def __init__(self):
        self.parser = reqparse.RequestParser(bundle_errors=True)

    @jwt_required()
    @teacher()
    def post(self, slug):
        section_validator(self.parser)
        args = self.parser.parse_args()
        SectionService.save_section(args, slug)
        return {}, http.HTTPStatus.CREATED


class TeacherSectionResource(Resource):

    def __init__(self):
        self.parser = reqparse.RequestParser(bundle_errors=True)

    @jwt_required()
    @teacher()
    def put(self, section_id):
        section_validator(self.parser)
        args = self.parser.parse_args()
        SectionService.update_section(args, section_id)
        return {}, http.HTTPStatus.OK

    @jwt_required()
    @teacher()
    def get(self, section_id):
        section = SectionService.get_section(section_id)
        return section, http.HTTPStatus.OK
