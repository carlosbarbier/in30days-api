from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource

from src.decorator.auth import teacher


class TeacherDashboardResource(Resource):
    @jwt_required()
    @teacher()
    def get(self):
        current_teacher = get_jwt_identity()
        teacher_id = current_teacher['id']
        try:
            return {}, 200
        except Exception as e:
            raise e
