import http

from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource, reqparse

from src.decorator.auth import teacher
from src.service.teacher.course_service import CourseService
from src.validation import course_creation_validator, course_update_validator


class TeacherCoursesResource(Resource):

    def __init__(self):
        self.parser = reqparse.RequestParser(bundle_errors=True)

    @jwt_required()
    @teacher()
    def get(self):
        current_teacher = get_jwt_identity()
        return CourseService.get_courses(current_teacher['id'])

    @jwt_required()
    @teacher()
    def post(self):
        course_creation_validator(self.parser)
        args = self.parser.parse_args()
        current_teacher = get_jwt_identity()
        CourseService.save_course(args, current_teacher)
        return {}, http.HTTPStatus.CREATED


class TeacherCourseResource(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser(bundle_errors=True)

    @jwt_required()
    @teacher()
    def get(self, slug):
        return CourseService.get(slug)

    @jwt_required()
    @teacher()
    def put(self, slug):
        course_update_validator(self.parser)
        args = self.parser.parse_args()
        CourseService.update(slug, args)
        return {}, http.HTTPStatus.OK

    def delete(self, slug):
        pass
