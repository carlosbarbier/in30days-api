"""Flask configuration."""
import os
from datetime import timedelta

from dotenv import load_dotenv

load_dotenv()

ACCESS_EXPIRES = timedelta(hours=24*7)


class Config:
    """Base config."""
    SECRET_KEY = os.getenv('SECRET_KEY')
    SESSION_COOKIE_NAME = os.getenv('SESSION_COOKIE_NAME')
    JWT_ERROR_MESSAGE_KEY = 'message'
    JWT_ACCESS_TOKEN_EXPIRES = ACCESS_EXPIRES


class ProdConfig(Config):
    JWT_ERROR_MESSAGE_KEY = 'message'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # SQLALCHEMY_DATABASE_URI = os.getenv('DB_CONNECTION') + '://' + os.getenv('POSTGRES_USER') + ':' + os.getenv(
    #     'POSTGRES_PASSWORD') + '@' + os.getenv('POSTGRES_HOST') + ':' + os.getenv('POSTGRES_PORT') + '/' + os.getenv(
    #     'POSTGRES_DB_NAME')
    JWT_ACCESS_TOKEN_EXPIRES = ACCESS_EXPIRES
    BUNDLE_ERRORS = True
    # s3
    AWS_SECRET_ACCESS_KEY = os.getenv('S3_KEY')
    FLASKS3_BUCKET_DOMAIN = os.getenv('S3_ENDPOINT')
    FLASKS3_BUCKET_NAME = os.getenv('S3_BUCKET')
    AWS_ACCESS_KEY_ID = os.getenv('AKIAVORZX3HKK3WQRO3F')
    FLASKS3_REGION = os.getenv('S3_REGION')

    # stripe
    STRIPE_SECRET_KEY = os.getenv('STRIPE_SECRET')
    STRIPE_PUBLIC_KEY = os.getenv('STRIPE_PUBLIC_KEY')

    # mail
    MAIL_SERVER = os.getenv('MAIL_SERVER')
    MAIL_PORT = os.getenv('MAIL_PORT')
    MAIL_USERNAME = os.getenv('MAIL_USERNAME')
    MAIL_PASSWORD = os.getenv('MAIL_PASSWORD')
    MAIL_USE_TLS = os.getenv('MAIL_USE_TLS')
    MAIL_USE_SSL = os.getenv('MAIL_USE_SSL')
    FLASK_ENV = 'production'
    DEBUG = False
    TESTING = False


class DevConfig(Config):
    FLASK_ENV = 'development'
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # SQLALCHEMY_DATABASE_URI = os.getenv('DB_CONNECTION') + '://' + os.getenv('POSTGRES_USER')+':' + os.getenv(
    #     'POSTGRES_PASSWORD') + '@' + os.getenv('POSTGRES_HOST') + ':' + os.getenv('POSTGRES_PORT') + '/' + os.getenv(
    #     'POSTGRES_DB_NAME')
    SQLALCHEMY_DATABASE_URI = os.getenv('DB_CONNECTION') + '://' + os.getenv('MYSQL_USER') + os.getenv(
        'MYSQL_PASSWORD') + '@' + os.getenv('MYSQL_HOST') + ':' + os.getenv('MYSQL_PORT') + '/' + os.getenv(
        'MYSQL_DB_NAME')

    JWT_ACCESS_TOKEN_EXPIRES = ACCESS_EXPIRES
    BUNDLE_ERRORS = True
    JWT_ERROR_MESSAGE_KEY = 'message'
    # s3
    AWS_SECRET_ACCESS_KEY = os.getenv('S3_KEY')
    FLASKS3_BUCKET_DOMAIN = os.getenv('S3_ENDPOINT')
    FLASKS3_BUCKET_NAME = os.getenv('S3_BUCKET')
    AWS_ACCESS_KEY_ID = os.getenv('AKIAVORZX3HKK3WQRO3F')
    FLASKS3_REGION = os.getenv('S3_REGION')

    # stripe
    STRIPE_SECRET_KEY = os.getenv('STRIPE_SECRET')
    STRIPE_PUBLIC_KEY = os.getenv('STRIPE_PUBLIC_KEY')

    # mail
    MAIL_SERVER = os.getenv('MAIL_SERVER')
    MAIL_PORT = os.getenv('MAIL_PORT')
    MAIL_USERNAME = os.getenv('MAIL_USERNAME')
    MAIL_PASSWORD = os.getenv('MAIL_PASSWORD')
    MAIL_USE_TLS = os.getenv('MAIL_USE_TLS')
    MAIL_USE_SSL = os.getenv('MAIL_USE_SSL')


config = {
    'development': DevConfig,
    'production': ProdConfig,
}
