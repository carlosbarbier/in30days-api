from datetime import datetime

import cv2
from flask_jwt_extended import get_current_user
from pydash import keys
from werkzeug.security import generate_password_hash

ALLOWED_IMAGE_EXTENSIONS = ['jpg', 'jpeg', 'png']
ALLOWED_VIDEO_EXTENSIONS = ['mp4', 'mov', 'wmv', 'flv']


def non_empty_string(s):
    if not s:
        raise ValueError("Must not be empty string")
    return s


def arrange_videos(data):
    if keys(data) == ['id', 'position'] or keys(data) == ['position', 'id']:
        return data
    else:
        raise ValueError("provided correct value")


def rating_value(rating):
    if not 1 <= rating <= 5:
        raise ValueError("Enter valid rating")
    return rating


def hash_password(password):
    return generate_password_hash(password)


def verify_sign_user():
    try:
        return get_current_user()
    except RuntimeError as e:
        print(e)


def validate_image_extension(file):
    return '.' in file.filename and file.filename.rsplit('.', 1)[1].lower() in ALLOWED_IMAGE_EXTENSIONS


def validate_video_extension(file):
    return '.' in file.filename and file.filename.rsplit('.', 1)[1].lower() in ALLOWED_VIDEO_EXTENSIONS


def image_url_builder(filename):
    return 'https://in30days-api.s3.eu-west-1.amazonaws.com/assets/images/' + filename


def image_key_builder(filename):
    return 'assets/images/' + filename


def video_url_builder(filename):
    return 'https://in30days-api.s3.eu-west-1.amazonaws.com/assets/videos/' + filename


def video_key_builder(filename):
    return 'assets/videos/' + filename


def current_time_ms():
    dt = datetime.now()
    return str(round(dt.timestamp() * 1000))


def get_video_duration(filename):
    cap = cv2.VideoCapture(filename)
    fps = cap.get(cv2.CAP_PROP_FPS)
    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    duration = frame_count / fps
    return duration
