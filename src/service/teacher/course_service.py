import ast
import os

from slugify import slugify
from sqlalchemy.exc import IntegrityError
from werkzeug.exceptions import InternalServerError, BadRequest, NotFound
from werkzeug.utils import secure_filename

from file import upload_file, delete_file
from src.error import CourseExistException
from src.util import validate_image_extension, current_time_ms, image_url_builder, image_key_builder


class CourseService:
    UPLOAD_FOLDER = "assets/images"

    @staticmethod
    def get_courses(teacher_id):
        from src.model import TeacherCourse
        from src.deserialiser import TeacherCoursesSchema
        try:
            teacher = TeacherCourse.query.filter_by(teacher_id=teacher_id)
            schema = TeacherCoursesSchema()
            dumped = schema.dump(teacher,many=True)
            for item in dumped:
                item['course']['things_to_learn'] = ast.literal_eval(item['course']['things_to_learn'])
            return dumped
        except Exception as e:
            raise InternalServerError('Internal server error')

    @staticmethod
    def save_course(args, current_teacher):
        from src.model import Course, TeacherCourse

        price = args['price']
        description = args['description']
        title = args['title']
        things_to_learn = args['things_to_learn']
        programming_language = args['programming_language']
        image = args['image']

        CourseService.__move_image_to_s3(image)
        slug = slugify(title)
        url = image_url_builder(image.filename)
        key = image_key_builder(image.filename)

        try:
            course = Course(title=title,
                            price=price,
                            description=description,
                            slug=slug,
                            url=url,
                            image_key=key,
                            things_to_learn=str(things_to_learn),
                            programming_language=programming_language,
                            author=current_teacher['name']
                            )
            course.save_to_db()
        except IntegrityError as e:
            raise CourseExistException()
        except Exception as e:
            raise InternalServerError("Internal Server")

        try:
            teacher_course = TeacherCourse(teacher_id=current_teacher['id'], course_id=course.id, total_earning=0, total_student=0)
            teacher_course.save_to_db()
        except Exception as e:
            raise InternalServerError("Internal Server")

    @staticmethod
    def get(slug):
        from src.deserialiser import CourseSchemaWithSection
        course = CourseService.__get_course_by_slug(slug)
        if course is not None:
            schema = CourseSchemaWithSection()
            dumped = schema.dump(course)
            dumped['things_to_learn'] = ast.literal_eval(dumped['things_to_learn'])
            return dumped
        else:
            raise NotFound("course not found")

    @staticmethod
    def update(slug, args):
        course = CourseService.__get_course_by_slug(slug)
        if course is not None:
            price = args['price']
            description = args['description']
            title = args['title']
            things_to_learn = args['things_to_learn']
            programming_language = args['programming_language']
            image = args['image']

            if price is not None:
                course.price = price
            if description is not None:
                course.description = description
            if title is not None:
                course.title = title
                slug = slugify(title)
                course.slug = slug
            if things_to_learn is not None:
                course.things_to_learn = things_to_learn
            if programming_language is not None:
                course.programming_language = programming_language
            if image is not None:
                CourseService.__move_image_to_s3(image)
                delete_file(course.image_key)
                url = image_url_builder(image.filename)
                key = image_key_builder(image.filename)
                course.url = url
                course.image_key = key
            course.save_to_db()
        else:
            raise NotFound("course not found")

    @staticmethod
    def __move_image_to_s3(image):
        if not validate_image_extension(image):
            raise BadRequest('Invalid image format')
        image.filename = current_time_ms() + '-' + image.filename
        try:
            image.save(os.path.join(CourseService.UPLOAD_FOLDER, secure_filename(image.filename)))
            response = upload_file(f"assets/images/{image.filename}")
            if response is None:
                os.remove(os.path.join(CourseService.UPLOAD_FOLDER, image.filename))
        except Exception as e:
            raise InternalServerError('an error occurred')

    @staticmethod
    def __get_course_by_slug(slug):
        from src.model import Course
        try:
            course = Course.find_by_slug(slug=slug)
        except Exception as e:
            raise InternalServerError('Internal server error')

        if course is not None:
            return course
        else:
            raise NotFound("course not found")
