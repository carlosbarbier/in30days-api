from werkzeug.exceptions import InternalServerError, NotFound

from src.error.section_exceede_exception import SectionExceedException
from src.service.helper import Helper


class SectionService:
    @staticmethod
    def save_section(args, slug):
        from src.model import Section
        course = Helper.get_course_by_slug(slug)
        wording = args['wording']
        section = Section(wording=wording, course_id=course.id)
        total = Helper.count_section(course.id)
        if total <= 30:
            try:
                section.save_to_db()
            except Exception as e:
                raise InternalServerError("Internal Server")
        else:
            raise SectionExceedException()

    @staticmethod
    def update_section(args, section_id):
        from src.model import Section
        wording = args['wording']
        try:
            section = Section.find_by_id(section_id)
        except Exception as e:
            raise InternalServerError("Internal Server")

        if section is not None:
            section.wording = wording
            try:
                section.save_to_db()
            except Exception as e:
                raise InternalServerError("Internal Server")
        else:
            raise NotFound("Section not found")

    @staticmethod
    def get_section(section_id):
        from src.model import Section
        from src.deserialiser import SectionSchema

        try:
            section = Section.find_by_id(section_id)
            schema = SectionSchema()
            dumped = schema.dump(section)
        except Exception as e:
            raise InternalServerError("Internal Server")

        if section is None:
            raise NotFound("Section not found")
        else:
            return dumped
