import os
from typing import List

from werkzeug.exceptions import BadRequest, InternalServerError, NotFound
from werkzeug.utils import secure_filename

from file import upload_file, delete_file
from src.dto.video_dto import VideoDto
from src.service.helper import Helper
from src.util import validate_video_extension, current_time_ms, video_url_builder, video_key_builder, get_video_duration


# This class is used to handle everything related to videos
# save video in the database
# edit the video
# delete the video
class VideoService:
    UPLOAD_FOLDER = "assets/videos"
    duration = 0

    @staticmethod
    def save_video(args):
        from src.model import Video
        title = args['title']
        section_id = args['section_id']
        video = args['video']
        # verifying if course exist in db
        section = Helper.get_section_by_id(section_id)
        course = Helper.get_course_by_id(section.course_id)

        # move video to s3
        VideoService.__move_video_to_s3(video)
        url = video_url_builder(video.filename)
        key = video_key_builder(video.filename)

        # saving video in database
        try:
            video = Video(title=title, section_id=section_id, url=url, video_key=key,
                          duration=VideoService.duration)
            video.save_to_db()
        except Exception as e:
            raise InternalServerError("Internal Server")

        #  updating course duration by adding the new video length
        try:
            if course.duration is None:
                course.duration = VideoService.duration
            else:
                course.duration = float(course.duration) + VideoService.duration
            course.save_to_db()
        except Exception as e:
            raise InternalServerError("Internal Server")

    @staticmethod
    def get_video(video_id):
        from src.model import Video
        from src.deserialiser import VideoSchema

        try:
            video = Video.query.get(video_id)
        except Exception as e:
            raise InternalServerError('Internal server error')

        if video is not None:
            schema = VideoSchema()
            dumped = schema.dump(video)
            return dumped
        else:
            raise NotFound("video not found")

    # update video information
    @staticmethod
    def update_video(args, video_id):
        video = Helper.get_video_by_id(video_id)
        if video is not None:
            title = args['title']
            video_file = args['video']
            day = args['day']
            if title is not None:
                video.title = title
            if day is not None:
                video.day = day
            if video_file is not None:
                VideoService.__move_video_to_s3(video_file)
                delete_file(video.video_key)
                url = video_url_builder(video_file.filename)
                key = video_key_builder(video_file.filename)
                video.url = url
                video.video_key = key
            try:
                video.save_to_db()
            except Exception as e:
                raise InternalServerError('Internal server error')
        else:
            raise NotFound("video not found")

    # delete video
    @staticmethod
    def delete_video(video_id):
        from src import db
        from src.model import Video

        video = Helper.get_video_by_id(video_id)
        if video is not None:
            delete_file(video.video_key)
            try:
                Video.query.filter_by(id=video_id).delete()
                db.session.commit()
            except Exception as e:
                raise InternalServerError('Internal server error')
        else:
            raise NotFound("video not found")

    # delete video
    @staticmethod
    def arrange_videos_positions(slug, args):
        from src.model import Video
        dto_list: List[VideoDto] = args['videos']
        if len(dto_list) > 0:
            Helper.get_course_by_slug(slug)
            for dto in dto_list:
                video = Video.find_by_id(dto['id'])
                video.position = dto['position']
                video.save_to_db()

    # private method to move the video to s3
    @staticmethod
    def __move_video_to_s3(video):
        if not validate_video_extension(video):
            raise BadRequest('Invalid video format')
        video.filename = current_time_ms() + '-' + video.filename
        try:
            video.save(os.path.join(VideoService.UPLOAD_FOLDER, secure_filename(video.filename)))
            VideoService.duration = get_video_duration(
                os.path.join(VideoService.UPLOAD_FOLDER, secure_filename(video.filename)))
            response = upload_file(f"assets/videos/{video.filename}")
            if response is None:
                os.remove(os.path.join(VideoService.UPLOAD_FOLDER, video.filename))
        except Exception as e:
            raise InternalServerError('an error occurred')
