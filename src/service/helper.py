from werkzeug.exceptions import InternalServerError, NotFound


class Helper:
    @staticmethod
    def get_course_by_slug(slug):
        from src.model import Course
        try:
            course = Course.find_by_slug(slug=slug)
        except Exception as e:
            raise InternalServerError('Internal server error')
        if course is not None:
            return course
        else:
            raise NotFound("course not found")

    @staticmethod
    def get_video_by_id(video_id):
        from src.model import Video
        try:
            video = Video.find_by_id(video_id)
        except Exception as e:
            raise InternalServerError('Internal server error')
        if video is not None:
            return video
        else:
            raise NotFound("video not found")

    @staticmethod
    def get_course_by_id(course_id):
        from src.model import Course
        try:
            course = Course.find_by_id(course_id)
        except Exception as e:
            raise InternalServerError('Internal server error')

        if course is not None:
            return course
        else:
            raise NotFound("course not found")

    @staticmethod
    def get_section_by_id(section_id):
        from src.model import Section
        try:
            course = Section.find_by_id(section_id)
        except Exception as e:
            raise InternalServerError('Internal server error')

        if course is not None:
            return course
        else:
            raise NotFound("Section not found")

    @staticmethod
    def get_question_answers(course_id):
        from src.model import StudentQa
        try:
            return StudentQa.query.filter(StudentQa.parent_id.is_(None), StudentQa.course_id == course_id).all()
        except Exception as e:
            raise InternalServerError('Internal server error')

    @staticmethod
    def count_section(course_id):
        from src.model import Section
        try:
            return Section.query.filter_by(course_id=course_id).count()
        except Exception as e:
            raise InternalServerError('Internal server error')

    # private method to get a specific video

    @staticmethod
    def get_video_by_id(video_id):
        from src.model import Video
        try:
            video = Video.find_by_id(video_id)
        except Exception as e:
            raise InternalServerError('Internal server error')
        if video is not None:
            return video
        else:
            raise NotFound("video not found")

    @staticmethod
    def get_course_owner(course_id: int):
        from src.model import TeacherCourse
        try:
            teacher_course = TeacherCourse.query.filter_by(course_id=course_id).first()
        except Exception as e:
            raise InternalServerError('Internal server error')
        if teacher_course is not None:
            return teacher_course
        else:
            raise NotFound("course not found")

    @staticmethod
    def check_if_user_has_enrolled(course_id, student_id):
        from src.model import StudentCourse
        try:
            student_course = StudentCourse.query.filter_by(course_id=course_id, student_id=student_id).first()
        except Exception as e:
            raise InternalServerError('Internal server error')
        if student_course is None:
            return False
        else:
            return True
