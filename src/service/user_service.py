from sqlalchemy.exc import IntegrityError
from werkzeug.exceptions import InternalServerError, BadRequest

from src.util import hash_password


class UserService:

    @staticmethod
    def save_user(args):
        from src.model import User
        user = User(name=args['name'], email=args['email'], password=hash_password(args['password']))
        try:
            user.save_to_db()
        except IntegrityError as e:
            raise BadRequest("email is taken")
        except Exception as e:
            raise InternalServerError("Internal Server")

    @staticmethod
    def get(self, id):
        pass

    @staticmethod
    def find_user_by_email(email, password):
        from src.deserialiser import UserSchema
        from src.model import User
        user = User.query.filter_by(email=email).first()
        if user is not None and user.verify_password(password):
            schema = UserSchema()
            return schema.dump(user)
        else:
            raise BadRequest("Invalid email or password")

    @staticmethod
    def verify_user(email, password):
        from src.deserialiser import UserSchema
        from src.model import User
        user = User.query.filter_by(email=email).first()
        if user is not None and user.verify_password(password):
            schema = UserSchema()
            return schema.dump(user)
        else:
            raise BadRequest("Invalid email or password")
