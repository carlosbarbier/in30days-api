import ast

from notification import send
from src.service.helper import Helper


class CourseService:

    @staticmethod
    def get_courses():
        from src.deserialiser import CourseSchemaWithoutQas
        from src.model import Course
        courses = Course.find_all()
        schema = CourseSchemaWithoutQas()
        dumped = schema.dump(courses, many=True)
        for item in dumped:
            item['things_to_learn'] = ast.literal_eval(item['things_to_learn'])
        return dumped

    @staticmethod
    def get_course(slug):
        from src.deserialiser import CourseSchemaWithoutQas
        course = Helper.get_course_by_slug(slug)
        schema = CourseSchemaWithoutQas()
        dumped = schema.dump(course)
        send()
        dumped['things_to_learn'] = ast.literal_eval(dumped['things_to_learn'])
        return dumped
