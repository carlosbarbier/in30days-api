import ast

from werkzeug.exceptions import InternalServerError

import payment
from src.error.enrollment_exception import EnrollmentException
from src.service.helper import Helper


class CourseService:

    @staticmethod
    def enroll_for_free_course(slug: str, student_id: int):
        course_found = Helper.get_course_by_slug(slug)
        user_has_enrolled = Helper.check_if_user_has_enrolled(course_found.id, student_id)
        if user_has_enrolled is not True:
            CourseService.__enroll_student(course_found, student_id)
        else:
            return

    @staticmethod
    def enroll_for_paid_course(args, slug, student_id: int):
        card_number = args['card_number']
        exp_month = args['exp_month']
        exp_year = args['exp_year']
        cvc = args['cvc']

        course_found = Helper.get_course_by_slug(slug)
        user_has_enrolled = Helper.check_if_user_has_enrolled(course_found.id, student_id)
        if user_has_enrolled is not True:
            course_owner = Helper.get_course_owner(course_found.id)
            # charge the student credit/debit card
            charge = payment.pay(float(course_found.price), course_found.title, card_number, exp_month, exp_year, cvc)
            # saving payment info to the database
            CourseService.__save_payment(student_id, course_owner.teacher_id, course_found.id, charge.stripe_id,
                                         course_found.price, charge.receipt_url)
            # update course owner earning and total student
            CourseService.__update_course_owner_earning(course_owner, course_found.price)

            # enroll the student
            CourseService.__enroll_student(course_found, student_id)
            course_found.total_student = int(course_found.total_student) + 1
            course_found.save_to_db()
        else:
            return

    @staticmethod
    def finish_course(student_id, slug):
        from src.model import StudentCourse
        course_found = Helper.get_course_by_slug(slug)
        try:
            student_course = StudentCourse.query.filter_by(student_id=student_id, course_id=course_found.id).first()
            student_course.is_completed = True
            student_course.save_to_db()
        except Exception as e:
            print(e)
            raise InternalServerError('Internal server error')

    @staticmethod
    def get_courses(student_id):
        from src.model import StudentCourse
        from src.deserialiser import StudentCourseSchema
        try:
            courses = StudentCourse.query.filter_by(student_id=student_id)
            schema = StudentCourseSchema()
            dumped = schema.dump(courses, many=True)
            for item in dumped:
                item['course']['things_to_learn'] = ast.literal_eval(item['course']['things_to_learn'])
            return dumped
        except Exception as e:
            raise InternalServerError('Internal server error')

    @staticmethod
    def get_course(student_id, slug):
        from src.model import StudentCourse
        from src.deserialiser import CourseSchemaWithSection, StudentQaSchema
        from src.service.student.video_service import VideoService
        found_course = Helper.get_course_by_slug(slug)

        try:
            course = StudentCourse.query.filter_by(student_id=student_id, course_id=found_course.id)
        except Exception as e:
            raise InternalServerError('Internal server error')

        if course is not None:
            qas = Helper.get_question_answers(course_id=found_course.id)
            video_ids = VideoService.get_watched_videos(course_id=found_course.id, student_id=student_id)
            schema = CourseSchemaWithSection()
            dumped = schema.dump(found_course)
            qa_schema = StudentQaSchema(many=True)
            dumped['qas'] = qa_schema.dump(qas)
            dumped['things_to_learn'] = ast.literal_eval(dumped['things_to_learn'])
            dumped['video_ids'] = video_ids
            return dumped
        else:
            raise EnrollmentException()

    @staticmethod
    def __save_payment(student_id, teacher_id, course_id, stripe_id, amount, receipt_url):
        from src.model import Payment
        payment_model = Payment(student_id=student_id, teacher_id=teacher_id,
                                course_id=course_id,
                                stripe_id=stripe_id, amount=float(amount), receipt_url=receipt_url)
        try:
            payment_model.save_to_db()
        except Exception as e:
            raise InternalServerError('Internal server error')

    @staticmethod
    def __update_course_owner_earning(course_owner, course_price):
        try:
            course_owner.total_earning = course_owner.total_earning + float(course_price)
            course_owner.total_student = course_owner.total_student + 1
            course_owner.save_to_db()
        except Exception as e:
            raise InternalServerError('Internal server error')

    @staticmethod
    def __enroll_student(course_found, student_id):
        from src.model import StudentCourse
        student_course = StudentCourse(course_id=course_found.id, is_paid=True, progress=0, student_id=student_id)
        try:
            student_course.save_to_db()
        except Exception as e:
            raise InternalServerError('Internal server error')
