from werkzeug.exceptions import InternalServerError, NotFound

from src.service.helper import Helper


class QaService:
    @staticmethod
    def save_qa(args, slug, student_id):
        from src.model import StudentQa
        parent_id = args['parent_id']
        body = args['body']
        course = Helper.get_course_by_slug(slug)

        if parent_id is not None:
            found = StudentQa.find_by_id(parent_id)
            print(found)
            if found is None:
                raise NotFound('Question with id ' + str(parent_id) + ' does not exist')
            if found.parent_id is None:
                student_qa = StudentQa(student_id=student_id, course_id=course.id, body=body, parent_id=found.id)
            else:
                student_qa = StudentQa(student_id=student_id, course_id=course.id, body=body, parent_id=found.parent_id)
        else:
            student_qa = StudentQa(student_id=student_id, course_id=course.id, body=body)

        try:
            student_qa.save_to_db()
        except Exception as e:
            raise InternalServerError('Internal server error')

    @staticmethod
    def like_qa():
        pass
