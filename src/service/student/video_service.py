from werkzeug.exceptions import InternalServerError, NotFound

from src.error.enrollment_exception import EnrollmentException
from src.service.helper import Helper


class VideoService:
    @staticmethod
    def finish_to_watch_video(slug, video_id, student_id):
        from src import db
        from src.model import StudentCourse
        from src.model import StudentVideo
        # verify if course exist in db
        course = Helper.get_course_by_slug(slug)
        if course:
            try:
                # ensure that the student is enrolled to the course
                student_course = StudentCourse.query.filter_by(student_id=student_id, course_id=course.id).first()
            except Exception as e:
                raise InternalServerError('Internal server error')

            if student_course is not None:
                # ensure that the video exist in db
                video = Helper.get_video_by_id(video_id)
                section = Helper.get_section_by_id(video.section_id)
                if section is not None and section.course_id == course.id:
                    if video is not None:
                        # ensure that the video is not already marked as watched
                        student_already_watched_the_video = StudentVideo.query.filter_by(student_id=student_id,
                                                                                         video_id=video.id).first()

                        if student_already_watched_the_video is None:

                            try:
                                # save student_video information in db
                                student_video = StudentVideo(student_id=student_id, video_id=video.id,course_id=course.id)
                                student_video.save_to_db()
                                # update student course progress information by adding the new video duration
                                progress = float(student_course.progress) + float(video.duration)
                                student_course.progress = progress
                                student_course.save_to_db()
                            except Exception as e:
                                raise InternalServerError('Internal server error')

                    else:
                        raise NotFound("video not found")
                else:
                    raise NotFound("video id mismatch with the course provided")
            else:
                raise EnrollmentException()

    @staticmethod
    def get_watched_videos(course_id, student_id):
        from src.deserialiser import StudentVideoSchema
        from src.model import StudentCourse
        from src.model import StudentVideo
        try:
            course = StudentCourse.query.filter_by(student_id=student_id, course_id=course_id).first()
        except Exception as e:
            raise InternalServerError('Internal server error')

        if course is not None:
            student_videos = StudentVideo.query.filter_by(student_id=student_id, course_id=course_id).all()
            schema = StudentVideoSchema(many=True)
            dumped = schema.dump(student_videos)
            return dumped
        else:
            raise EnrollmentException()
