from werkzeug.exceptions import InternalServerError

from src.error.enrollment_exception import EnrollmentException
from src.service.helper import Helper


class RatingService:
    @staticmethod
    def save_rating(args, slug, student_id):
        from src.model import StudentRating
        rating = args['rating']
        rating_message = args['rating_message']
        course = Helper.get_course_by_slug(slug)
        user_has_enrolled = Helper.check_if_user_has_enrolled(course.id, student_id=student_id)
        if user_has_enrolled is not True:
            raise EnrollmentException()
        try:
            course_rating = StudentRating(id=1, rating=rating, rating_message=rating_message, course_id=course.id,
                                          student_id=student_id)
            course_rating.save_to_db()
        except Exception as e:
            raise InternalServerError('Internal server error')

    @staticmethod
    def like_qa():
        pass
