from flask_restful import inputs
from werkzeug.datastructures import FileStorage

from src.util import non_empty_string, rating_value, arrange_videos


def user_registration_validator(parser):
    parser.add_argument('name', help='valid name required', required=True, nullable=False, type=non_empty_string,
                        trim=True)
    parser.add_argument('email', help='valid email required',
                        type=inputs.regex(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"), nullable=False)
    parser.add_argument('password', help='valid password required', required=True, nullable=False,
                        type=non_empty_string)


def user_login_validator(parser):
    parser.add_argument('email', help='valid email required',
                        type=inputs.regex(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"), nullable=False)
    parser.add_argument('password', help='valid password required', required=True, nullable=False,
                        type=non_empty_string)


def course_creation_validator(parser):
    parser.add_argument('title', help='title required', required=True, nullable=False, type=non_empty_string)
    parser.add_argument('price', help='price required', required=True, nullable=False, type=non_empty_string)
    parser.add_argument('description', help='description required', required=True, nullable=False,
                        type=non_empty_string)

    parser.add_argument('things_to_learn', help='things to learn required', required=True, nullable=False,
                        action='append')
    parser.add_argument('programming_language', help='programming languages required', required=True, nullable=False,
                        action='append')
    parser.add_argument('image', type=FileStorage, location='files', help='image  required', required=True,
                        nullable=False)


def course_update_validator(parser):
    parser.add_argument('title', help='title can not be null or empty', nullable=False, type=non_empty_string)
    parser.add_argument('price', help='price required', nullable=False, type=non_empty_string)
    parser.add_argument('description', help='description required', nullable=False,
                        type=non_empty_string)

    parser.add_argument('things_to_learn', help='things to learn required', nullable=False,
                        type=non_empty_string)
    parser.add_argument('programming_language', help='programming languages required', required=False, nullable=False,
                        type=non_empty_string)
    parser.add_argument('image', type=FileStorage, location='files', help='image  required', required=False,
                        nullable=False)


def video_creation_validator(parser):
    parser.add_argument('title', help='valid title required', nullable=False, required=True, type=non_empty_string)
    parser.add_argument('section_id', help='valid course id required', nullable=False, required=True,
                        type=non_empty_string)
    parser.add_argument('video', type=FileStorage, location='files', help='video file required', required=True,
                        nullable=False)


def video_update_validator(parser):
    parser.add_argument('title', help='valid title required', nullable=False, type=non_empty_string)
    parser.add_argument('day', help='valid day number required', nullable=False, type=non_empty_string)
    parser.add_argument('video', type=FileStorage, location='files', help='video file required',
                        nullable=False)


def section_validator(parser):
    parser.add_argument('wording', help='valid title required', nullable=False, type=non_empty_string, required=True)


def qa_validator(parser):
    parser.add_argument('body', help='valid body required', nullable=False, type=non_empty_string, required=True)
    parser.add_argument('parent_id', help='valid question parent id  required', nullable=True, type=non_empty_string)


def rating_validator(parser):
    parser.add_argument('rating', help='valid rating required', nullable=False, type=rating_value, required=True)
    parser.add_argument('rating_message', help='valid rating message required', nullable=False, type=non_empty_string,
                        required=True)


def videos_arrangement_validator(parser):
    parser.add_argument('videos', help='required object of id and position', required=True, nullable=False,
                        action='append', type=arrange_videos)


def card_validator(parser):
    parser.add_argument('card_number', help='card number required', required=True, nullable=False,
                        type=non_empty_string)
    parser.add_argument('exp_month', help='expiration month required', required=True, nullable=False,
                        type=int)
    parser.add_argument('exp_year', help='expiration year required', required=True, nullable=False,
                        type=int)
    parser.add_argument('cvc', help='cvc required', required=True, nullable=False,
                        type=non_empty_string)
