from dataclasses import dataclass


@dataclass
class VideoDto:
    id: int
    position: int
