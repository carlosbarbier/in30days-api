from flask_restful import Api

from src.resource.course import CoursesResource, CourseResource
from src.resource.student import student_dashboard, student_course, student_video, student_qa, student_rating
from src.resource.teacher import teacher_video, teacher_course, teacher_section
from src.resource.user import register, login,logout


def register_routes(app):
    api = Api(app)
    api.add_resource(register.UserRegisterResource, '/api/users/register')
    api.add_resource(login.UserLoginResource, '/api/users/login')
    api.add_resource(logout.UserLogoutResource, '/api/users/logout')
    api.add_resource(CoursesResource, '/api/courses')
    api.add_resource(CourseResource, '/api/courses/<string:slug>')

    api.add_resource(teacher_course.TeacherCoursesResource, '/api/teacher/courses')
    api.add_resource(teacher_course.TeacherCourseResource, '/api/teacher/courses/<string:slug>')
    api.add_resource(teacher_section.TeacherSectionsResource, '/api/teacher/courses/<string:slug>/sections')
    api.add_resource(teacher_section.TeacherSectionResource, '/api/teacher/sections/<int:section_id>/')
    api.add_resource(teacher_video.TeacherVideosResource, '/api/teacher/videos')
    api.add_resource(teacher_video.TeacherVideosPositionsResource, '/api/teacher/courses/<string:slug>/videos/arrange')
    api.add_resource(teacher_video.TeacherVideoResource, '/api/teacher/videos/<string:video_id>')

    api.add_resource(student_dashboard.DashboardResource, '/api/students/dashboard')
    api.add_resource(student_course.StudentFreeEnrollmentResource, '/api/student/courses/<string:slug>',
                     '/api/student/courses/<string:slug>/free-enrollment', )
    api.add_resource(student_course.StudentCourseResource, '/api/student/courses/<string:slug>')
    api.add_resource(student_course.StudentPaidEnrollmentResource,'/api/student/courses/<string:slug>/paid-enrollment', )
    api.add_resource(student_course.StudentFinishCourseResource,'/api/student/courses/<string:slug>/complete', )

    api.add_resource(student_course.StudentCoursesResource, '/api/student/courses')

    api.add_resource(student_video.StudentVideosResource, '/api/student/courses/<string:slug>/videos/<string:video_id>')
    api.add_resource(student_video.StudentVideoResource, '/api/student/videos',
                     '/api/student/courses/<string:course_id>/videos/watched')
    api.add_resource(student_rating.StudentRatingsResource, '/api/student/courses/<string:slug>/ratings')
    api.add_resource(student_qa.StudentQasResource, '/api/student/courses/<string:slug>/qas')
