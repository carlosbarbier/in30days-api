from datetime import datetime, timezone, timedelta

from flask import Flask
from flask_cors import CORS
from flask_jwt_extended import JWTManager, get_jwt, set_access_cookies, get_jwt_identity, create_access_token
from flask_mail import Mail
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from cache import redis_con
from src.config import config
from src.error import GlobalException
from src.routes import register_routes


def create_app():
    app = Flask(__name__)

    # config properties
    app.config.from_object(config.get('development'))

    # database
    db.init_app(app)

    # migration
    migrate.init_app(app, db)
    from src.model import User
    from src.model import Course
    from src.model import Section
    from src.model import Video
    from src.model import StudentCourse
    from src.model import QaLike
    from src.model import StudentQa
    from src.model import StudentRating
    from src.model import StudentVideo
    from src.model import TeacherCourse
    from src.model import Payment

    # error
    GlobalException.handle(app)

    # jwt
    jwt.init_app(app)
    redis_con.set("test", "test")

    @jwt.token_in_blocklist_loader
    def check_if_token_is_revoked(jwt_header, jwt_payload):
        jti = jwt_payload["jti"]
        token_in_redis = redis_con.get(jti)
        return token_in_redis is not None

    # routes
    register_routes(app)

    # cors
    cors_config = {
        "origins": ["http://localhost:3000"]
    }
    CORS(app, resources={"/api/*": cors_config})

    # s3
    mail.init_app(app)

    @app.after_request
    def refresh_expiring_jwts(response):
        try:
            exp_timestamp = get_jwt()["exp"]
            now = datetime.now(timezone.utc)
            target_timestamp = datetime.timestamp(now + timedelta(minutes=30))
            if target_timestamp > exp_timestamp:
                access_token = create_access_token(identity=get_jwt_identity())
                set_access_cookies(response, access_token)
            return response
        except (RuntimeError, KeyError):
            return response

    return app


db = SQLAlchemy()
migrate = Migrate()
jwt = JWTManager()
mail = Mail()
