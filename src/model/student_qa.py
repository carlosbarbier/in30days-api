from sqlalchemy import Column

from src import db
from src.model.base_model import BaseModel


class StudentQa(BaseModel):
    __tablename__ = 'student_qas'
    student_id = Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    course_id = db.Column(db.Integer, db.ForeignKey('courses.id'), nullable=False)
    parent_id = db.Column(db.Integer, db.ForeignKey('student_qas.id'), nullable=True)
    body = db.Column(db.String(255))
    parent = db.relationship("StudentQa",  remote_side='StudentQa.id', backref='replies')
