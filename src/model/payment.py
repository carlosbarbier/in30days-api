from sqlalchemy import Column

from src import db
from src.model.base_model import BaseModel


class Payment(BaseModel):
    __tablename__ = 'payments'

    teacher_id = Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    student_id = Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    course_id = db.Column(db.Integer, db.ForeignKey('courses.id'), nullable=False)
    stripe_id = db.Column(db.String(255))
    amount = db.Column(db.Float, default=True)
    receipt_url = db.Column(db.String(255))
    is_refunded = db.Column(db.BOOLEAN, default=False)
