from sqlalchemy import Column

from src import db
from src.model.base_model import BaseModel


class TeacherCourse(BaseModel):
    __tablename__ = 'teacher_courses'

    teacher_id = Column(db.Integer, db.ForeignKey('users.id'), nullable=True)
    course_id = db.Column(db.Integer, db.ForeignKey('courses.id'), nullable=True)
    total_earning = db.Column(db.Float, default=True)
    total_student = db.Column(db.Float, default=True)
    course = db.relationship('Course', backref='course', )

