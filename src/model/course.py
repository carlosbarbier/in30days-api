from src import db

from src.model.base_model import BaseModel


class Course(BaseModel):

    __tablename__ = 'courses'

    title = db.Column(db.String(255), nullable=False)
    price = db.Column(db.String(10), nullable=False)
    slug = db.Column(db.String(255), nullable=False, unique=True)
    description = db.Column(db.String(255), nullable=False)
    is_ready = db.Column(db.BOOLEAN, nullable=False, default=False)
    programming_language = db.Column(db.String(255), nullable=False)
    url = db.Column(db.String(255), nullable=True)
    image_key = db.Column(db.String(255), nullable=True)
    duration = db.Column(db.String(20), nullable=True)
    total_student = db.Column(db.Integer, default=0)
    author = db.Column(db.String(100), nullable=True)
    things_to_learn = db.Column(db.String(255), nullable=False)
    sections = db.relationship('Section', backref='courses', order_by='Section.position.asc()')

