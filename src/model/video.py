from src import db
from src.model.base_model import BaseModel


class Video(BaseModel):
    __tablename__ = 'videos'

    title = db.Column(db.String(255), nullable=False)
    position = db.Column(db.Integer(), default=0)
    url = db.Column(db.String(255), nullable=True)
    video_key = db.Column(db.String(255), nullable=True)
    duration = db.Column(db.String(20), nullable=True)
    section_id = db.Column(db.Integer, db.ForeignKey('sections.id'), nullable=False)
