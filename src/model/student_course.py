
from sqlalchemy import Column

from src import db
from src.model.base_model import BaseModel


class StudentCourse(BaseModel):
    __tablename__ = 'student_courses'

    student_id = Column(db.Integer, db.ForeignKey('users.id'), nullable=True)
    course_id = db.Column(db.Integer, db.ForeignKey('courses.id'), nullable=False)
    is_completed = db.Column(db.BOOLEAN, default=False)
    is_paid = db.Column(db.BOOLEAN, default=True)
    is_active = db.Column(db.BOOLEAN, default=True)
    progress = db.Column(db.Float)
    course = db.relationship('Course', backref='course_id', )

