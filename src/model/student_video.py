
from sqlalchemy import Column

from src import db
from src.model.base_model import BaseModel


class StudentVideo(BaseModel):
    __tablename__ = 'student_videos'
    student_id = Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    video_id = db.Column(db.Integer, db.ForeignKey('videos.id'), nullable=False)
    course_id = db.Column(db.Integer, db.ForeignKey('courses.id'), nullable=False)
