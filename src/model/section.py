from src import db
from src.model.base_model import BaseModel


class Section(BaseModel):
    __tablename__ = 'sections'

    wording = db.Column(db.String(150), nullable=False)
    position = db.Column(db.Integer(), default=0)
    course_id = db.Column(db.Integer, db.ForeignKey('courses.id'), nullable=False)
    videos = db.relationship('Video', backref='videos')