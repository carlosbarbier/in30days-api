from sqlalchemy import Column

from src import db
from src.model.base_model import BaseModel


class QaLike(BaseModel):
    __tablename__ = 'qa_likes'
    student_qa_id = Column(db.Integer, db.ForeignKey('student_qas.id'), nullable=False,primary_key=True)
    like = db.Column(db.Integer, nullable=False, default=0)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False,primary_key=True)
