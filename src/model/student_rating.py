
from sqlalchemy import Column

from src import db
from src.model.base_model import BaseModel


class StudentRating(BaseModel):
    __tablename__ = 'student_ratings'

    student_id = Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    course_id = db.Column(db.Integer, db.ForeignKey('courses.id'), nullable=False)
    rating = db.Column(db.Integer)
    rating_message = db.Column(db.String(255))

