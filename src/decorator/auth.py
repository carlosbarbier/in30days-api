from functools import wraps

import flask_restful
from flask_jwt_extended import get_jwt_identity


def student():
    def _student(f):
        @wraps(f)
        def __student(*args, **kwargs):
            current_user = get_jwt_identity()
            if current_user is not None and current_user['role'] == 'user':
                return f(*args, **kwargs)
            flask_restful.abort(http_status_code=401, message="not authorized")
        return __student
    return _student


def teacher():
    def _teacher(f):
        @wraps(f)
        def __teacher(*args, **kwargs):
            current_user = get_jwt_identity()
            if current_user is not None and current_user['role'] == 'teacher':
                return f(*args, **kwargs)
            flask_restful.abort(http_status_code=401, message="not authorized")
        return __teacher
    return _teacher
