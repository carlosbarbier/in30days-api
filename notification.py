import os
import smtplib
import ssl

context = ssl.create_default_context()


def send(message):
    try:
        context = ssl.create_default_context()
        with smtplib.SMTP(os.getenv('MAIL_SERVER'), os.getenv('MAIL_PORT')) as server:
            server.starttls(context=context)
            server.login(os.getenv('MAIL_USERNAME'), os.getenv('MAIL_PASSWORD'))
            server.sendmail("codeavecmoi@gmail.com", "codeavecmoi@gmail.com", message)
        return 'Sent'
    except Exception as e:
        print(e)
